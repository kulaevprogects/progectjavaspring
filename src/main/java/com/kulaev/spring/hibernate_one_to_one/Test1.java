package com.kulaev.spring.hibernate_one_to_one;

import com.kulaev.spring.hibernate_one_to_one.entity.Detail;
import com.kulaev.spring.hibernate_one_to_one.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test1 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Detail.class)
                .buildSessionFactory();
        Session session =null;
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Employee employee = session.get(Employee.class,2);
            session.delete(employee);
            session.getTransaction().commit();
            System.out.println("All done well");



           // session = factory.getCurrentSession();
            //session.beginTransaction();
           // Employee employee = session.get(Employee.class,20);
           // System.out.println(employee.getEmployeeDetail());
           // session.getTransaction().commit();
           // System.out.println("All done well");



            //Session session = factory.getCurrentSession();
           // Employee employee = new Employee("Dmitriy","Ivanov","Security",100);
            //Detail detail = new Detail("Nizniy Novgorod","8773923456","saniazxc@gmail.com");
           // employee.setEmployeeDetail(detail);
           // session.beginTransaction();
            //session.save(employee);
            //session.getTransaction().commit();
            //System.out.println("All done well");
        }
        finally {
            session.close();
            factory.close();
        }
    }
}
