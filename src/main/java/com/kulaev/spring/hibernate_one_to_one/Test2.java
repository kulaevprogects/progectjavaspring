package com.kulaev.spring.hibernate_one_to_one;

import com.kulaev.spring.hibernate_one_to_one.entity.Detail;
import com.kulaev.spring.hibernate_one_to_one.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test2 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Detail.class)
                .buildSessionFactory();
        Session session =null;
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Detail detail = session.get(Detail.class,1);
            detail.getEmployee().setEmployeeDetail(null);
            session.delete(detail);
            session.getTransaction().commit();
            System.out.println("All done well");

            //session = factory.getCurrentSession();
           // session.beginTransaction();
            //Detail detail = session.get(Detail.class,4);
            //System.out.println(detail.getEmployee());
            //session.getTransaction().commit();
            //System.out.println("All done well");


            //session = factory.getCurrentSession();
            //Employee employee = new Employee("Tom","Johnson","HR",500);
            //Detail detail = new Detail("Chicago","8111111456","TomFromChicago@mail.ru");
            //employee.setEmployeeDetail(detail);
            //detail.setEmployee(employee);
            //session.beginTransaction();
           // session.save(detail);
            //session.getTransaction().commit();
            //System.out.println("All done well");
        }
        finally {
            session.close();
            factory.close();
        }
    }
}
