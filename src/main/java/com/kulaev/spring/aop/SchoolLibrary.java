package com.kulaev.spring.aop;

import com.kulaev.spring.aop.AbstractLibrary;
import org.springframework.stereotype.Component;

@Component
public class SchoolLibrary extends AbstractLibrary {

    public void getBook() {
        System.out.println("Мы берем книгу из школьной библиотеки");
    }
}
