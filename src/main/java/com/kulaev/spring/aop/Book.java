package com.kulaev.spring.aop;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Book{
    @Value(" Слово о полку Игореве")
    private String name;
    @Value("Не установлен")
    private String author;
    @Value("1185")
    private int yearofPublication;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public int getYearofPublication() {
        return yearofPublication;
    }
}
