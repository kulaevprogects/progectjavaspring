package com.kulaev.spring.aop;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class University {
    private List<Student> studentList = new ArrayList<>();
    public void addStudents(){
        Student st1 = new Student("Kulaev Sergey", 4,8.5);
        Student st2 = new Student("Tolstoy Lev", 3,7.5);
        Student st3 = new Student("Pushkin Alexander ", 2,2.5);
        this.studentList.add(st1);
        this.studentList.add(st2);
        this.studentList.add(st3);
    }

    public List<Student> getStudentList() {
        System.out.println("Начало работы метода getStudentList ");
        //System.out.println(studentList.get(3));
        System.out.println("Information from method getStusentList:  ");
        System.out.println(studentList);
        return studentList;
    }
}
