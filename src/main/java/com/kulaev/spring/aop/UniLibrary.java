package com.kulaev.spring.aop;

import org.springframework.stereotype.Component;

@Component
public class UniLibrary extends  AbstractLibrary {

   // public void getBook(Book book){

      //  System.out.println("Мы берем книгу из университетской библиотеки" + book.getName());
   // }
    public void getBook(){

        System.out.println("Мы берем книгу из университетской библиотеки" );
        System.out.println("----------------------------------------------------------------------");
    }
    protected String returnBook(){
        int a = 10/0;
        System.out.println("Мы возвращаем книгу в университетскую  библиотеку");
        return "Слово о полку Игореве";
    }
    public void getMagazine(){
        System.out.println("Мы берем журнал из университетской библиотеки");
        System.out.println("----------------------------------------------------------------------");
    }
    public void returnMagazine(){
        System.out.println("Мы возвращаем журнал в университетскую  библиотеку");
        System.out.println("----------------------------------------------------------------------");
    }
    public void addBook(String person_Name, Book book){

        System.out.println("Мы добавляем книгу в университетскую библиотеку" );
        System.out.println("----------------------------------------------------------------------");
    }
    public void addMagazine(){

        System.out.println("Мы добавляем журнал в университетскую библиотеку" );
        System.out.println("----------------------------------------------------------------------");
    }
}
