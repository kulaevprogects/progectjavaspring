package com.kulaev.spring.aop.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(20)
public class SecurityAspect {
    @Before("com.kulaev.spring.aop.aspects.MyPointcuts.allAddMethods()")
    public void BeforeAddSecurityAdvice(){
        System.out.println("BeforeAddSecurityAdvice:проверка прав на получение книги/журнала");
        System.out.println("----------------------------------------------------------------------");
    }
}
