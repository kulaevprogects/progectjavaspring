package com.kulaev.spring.aop.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class NewLoggingAspect {
    @Around("execution(* returnBook())")
        public Object AroundReturnBookLoggingAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("AroundReturnBookLoggingAdvice: в библиотеку "+"пытаются вернуть книгу");
        long begin = System.currentTimeMillis();
        Object targetMethodResult = null;
        try {
             targetMethodResult = joinPoint.proceed();
        }
        catch (Exception e){
            System.out.println("AroundReturnBookLoggingAdvice:было поймано "
                    +" исключение "+e);
            throw e;

        }
        long end  = System.currentTimeMillis();
        System.out.println("AroundReturnBookLoggingAdvice: в библиотеку "+"успешно вернули  книгу");
        System.out.println("AroundReturnBookLoggingAdvice: метод returnBook выполнил работу за   "+(end-begin)+"  миллисекунд");
        return targetMethodResult;
    }
}
