package com.kulaev.spring.aop.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(30)
public class ExceptionHandlingAspect {
    @Before("com.kulaev.spring.aop.aspects.MyPointcuts.allAddMethods()")
    public void BeforeAddExceptionHandlingAdvice(){
        System.out.println("BeforeAddExceptionHandlingAdvice:ловим/обрабатываем" + "исключения при попытке получть книгу/журнал");
        System.out.println("----------------------------------------------------------------------");
    }
}
