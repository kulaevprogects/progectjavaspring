package com.kulaev.spring.aop.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class UniversityLoggingAspect {
    @After("execution(* get*(..))")
    public void AfterGetStudentsLoggingAdvice (){
        System.out.println("AfterGetStudentsLoggingAdvice:логгируем нормальное"+ "  окончание работы метода или выброс исключения");
    }
}