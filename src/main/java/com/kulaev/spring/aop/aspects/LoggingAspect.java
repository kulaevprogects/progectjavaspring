package com.kulaev.spring.aop.aspects;

import com.kulaev.spring.aop.Book;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(10)
public class LoggingAspect {
    @Before("com.kulaev.spring.aop.aspects.MyPointcuts.allAddMethods()")
    public void BeforeAddLoggingAdvice(JoinPoint joinPoint){
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        System.out.println(" MethodSignature "+  methodSignature);
        System.out.println(" MethodSignature.getMethod "+  methodSignature.getMethod());
        System.out.println(" MethodSignature.getReturnType()"+  methodSignature.getReturnType());
        System.out.println(" MethodSignature.getName()"+  methodSignature.getName());

        if(methodSignature.getName().equals(("addBook"))){
            Object [] arguments = joinPoint.getArgs();
            for (Object obj:arguments){
                if(obj instanceof Book){
                    Book myBook = (Book) obj;
                    System.out.println("Информация о книге:название книги -  " + myBook.getName());
                    System.out.println("Информация о книге:автор книги  -  " + myBook.getAuthor());
                    System.out.println("Информация о книге:год написания книги -  " + myBook.getYearofPublication());
                }
                else if(obj instanceof String){
                    String person = (String) obj;
                    System.out.println("Книгу в библиотеку принес  " + person);

                }            }
        }
        System.out.println("BeforeAddBookAdvice:логирование" + " попытки взять книгу/журнал");
        System.out.println("----------------------------------------------------------------------");
    }
    // @Before("execution(public void com.kulaev.spring.aop.UniLibrary.getBook())")
    // public void BeforeGetBookAdvice(){
    //   System.out.println("BeforeGetBookAdvice:попытка взять книгу");
    //}
    //@Before("execution(public void getBook(..))")
    // public void BeforeGetBookAdvice(){
    //   System.out.println("BeforeGetBookAdvice:попытка взять книгу");
    //}
    // @Before("execution( * returnBook())")
    // public void BeforeReturnBookAdvice(){
    //     System.out.println("BeforeReturnBookAdvice:попытка вернуть книгу");
    // }
























  //  @Pointcut("execution(* com.kulaev.spring.aop.UniLibrary.*(..))")
 //   private void allMethodsFromUniLibrary(){}

  //  @Pointcut("execution(public void com.kulaev.spring.aop.UniLibrary.returnMagazine())")
  //  private void returnMagazineFromUniLibrary(){}
  //  @Pointcut("allMethodsFromUniLibrary() && !returnMagazineFromUniLibrary())")
   // private void allMethodsExceptReturnfromUniLibrary(){}
  //  @Before("allMethodsExceptReturnfromUniLibrary()")
  //  public void beforeALLMethodsExceptReturnMagazineAdvice(){
   //     System.out.println("beforeALLMethodsExceptReturnMagazineAdvice: Log#10");
   // }













    //@Pointcut("execution(* com.kulaev.spring.aop.UniLibrary.get*())")
    //private void allGetMethodsFromUniLibrary(){}
    //@Pointcut("execution(* com.kulaev.spring.aop.UniLibrary.return*())")
    //private void allReturnMethodsFromUniLibrary(){}
   // @Pointcut("allGetMethodsFromUniLibrary() || allReturnMethodsFromUniLibrary()")
    //private void allReturnandGetMethodsFromUniLibrary(){}
    //@Pointcut("execution(* com.kulaev.spring.aop.UniLibrary.add*())")
    //private void allAddMethodsFromUniLibrary(){}
    //@Before("allGetMethodsFromUniLibrary()")
   // public void beforeGetLoggingAdvice(){
    //    System.out.println("beforeGetLoggingAdvice:writing Log#1");
    //}
    //@Before("allReturnMethodsFromUniLibrary()")
    //public void beforeReturnLoggingAdvice(){
     //   System.out.println("beforeReturnLoggingAdvice:writing Log#2");
    //}
    //@Before("allAddMethodsFromUniLibrary()")
    //public void beforeAddLoggingAdvice(){
        //System.out.println("beforeAddLoggingAdvice:writing Log#4");
    //}
    //@Before("allReturnandGetMethodsFromUniLibrary()")
    //public void beforeGetAndReturnLoggingAdvice(){
     //   System.out.println("beforeGetAndReturnLoggingAdvice:writing Log#3");
    //    }

}
















