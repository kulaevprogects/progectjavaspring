package com.kulaev.spring.spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeTest {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext3");
        Cat myCat = context.getBean("catBean",Cat.class);
        myCat.say();
        //Cat yourCat = context.getBean("catBean",Cat.class);
        //System.out.println("Перременнные ссылаются на один и тот же объект?"+(myCat==yourCat));
        //System.out.println(myCat);
        //System.out.println(yourCat);
        //context.close();
    }
}