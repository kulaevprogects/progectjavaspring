package com.kulaev.spring.spring_introduction;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Component("catBean")
public class Cat implements Pet{
//    private String name;
    public Cat(){
        System.out.println("Cat is created");
    }
    @Override
    public void say() {
        System.out.println("Meow-Meow");
    }
    //@PostConstruct
    //protected void init (){
        //System.out.println("Cat:init method");
    //}
    //@PreDestroy
    //private void destroy (){
        //System.out.println("Cat:destroy method");
    //}

 //   public String getName() {
//        return name;
 //   }

 //   public void setName(String name) {
 //       this.name = name;
  //  }
}
