package com.kulaev.spring.spring_introduction;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test6 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =new AnnotationConfigApplicationContext(Myconfig.class);
        //Pet cat1 = context.getBean("catBean",Pet.class);
        //Pet cat2 = context.getBean("catBean",Pet.class);
        //cat.say();
        Person person = context.getBean("personBean",Person.class);
        person.CallYourPet();
        System.out.println(person.getSurname());
        System.out.println(person.getAge());
        context.close();
    }
}
