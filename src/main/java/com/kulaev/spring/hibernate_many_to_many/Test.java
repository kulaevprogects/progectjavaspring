package com.kulaev.spring.hibernate_many_to_many;

import com.kulaev.spring.hibernate_many_to_many.Entity.Child;
import com.kulaev.spring.hibernate_many_to_many.Entity.Section;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.*;


public class Test
{
    public static void main(String[] args) {
        SessionFactory factory =new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Child.class)
                .addAnnotatedClass(Section.class)
                .buildSessionFactory();
        Session session = null;
        try {
//            session=  factory.getCurrentSession();
//            Section section = new Section("Dance");
//            Child child1 = new Child("Kolya",12);
//            Child child2 = new Child("Volya",12);
//            Child child3 = new Child("Tolya",10);
//            section.addChildToSection(child1);
//            section.addChildToSection(child2);
//            section.addChildToSection(child3);
//            session.beginTransaction();
//            session.persist(section);
//            session.getTransaction().commit();
//            System.out.println("All done well");
            //*****************************************************************************************************
//            session=  factory.getCurrentSession();
//            Section section1 = new Section("Football");
//            Section section2 = new Section("Voleyball");
//            Section section3 = new Section("BasketBall");
//            Child child1 = new Child("Anton",16);
//            child1.addSectionToChild(section1);
//            child1.addSectionToChild(section2);
//            child1.addSectionToChild(section3);
//            session.beginTransaction();
//            session.save(child1);
//            session.getTransaction().commit();
//            System.out.println("All done well");
            //*****************************************************************************************************
//            session=  factory.getCurrentSession();
//            session.beginTransaction();
//            Section section = session.get(Section.class, 2);
//            System.out.println(section);
//            System.out.println(section.getChildren());
//            session.getTransaction().commit();
//            System.out.println("All done well");
            //*****************************************************************************************************
//            session=  factory.getCurrentSession();
//            session.beginTransaction();
//            Child child = session.get(Child.class, 4);
//            System.out.println(child);
//            System.out.println(child.getSections());
//            session.getTransaction().commit();
//            System.out.println("All done well");
            //*****************************************************************************************************
//            session=  factory.getCurrentSession();
//            session.beginTransaction();
//            Section section = session.get(Section.class, 8);
//            session.delete(section);
//            session.getTransaction().commit();
//            System.out.println("All done well");
            //*****************************************************************************************************
            session=  factory.getCurrentSession();
            session.beginTransaction();
            Child child = session.get(Child.class, 5);
            session.delete(child);
            session.getTransaction().commit();
            System.out.println("All done well");
        }
        finally {
            session.close();
            factory.close();

        }
    }
}
