package com.kulaev.spring.hibernate_test;

import com.kulaev.spring.hibernate_test.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test4 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();
        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();
            session.createQuery("update Employee set salary=1000 where name = 'Dmitriy'").executeUpdate();
           // Employee employee = session.get(Employee.class,1);
            //employee.setDepatrment("IT");
            session.getTransaction().commit();
            System.out.println("All done well");
        } finally {
            factory.close();
        }
    }
}
