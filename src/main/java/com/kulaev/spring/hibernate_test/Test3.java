package com.kulaev.spring.hibernate_test;

import com.kulaev.spring.hibernate_test.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test3 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();
        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();
       //     List<Employee> employees= session.createQuery("from Employee").getResultList();
            List<Employee> employees= session.createQuery("from Employee"+" where name='Vladimir' AND salary>1000").getResultList();
            for(Employee emp:employees){
                System.out.println(emp);
            }
            session.getTransaction().commit();
            System.out.println("All done well");
        } finally {
            factory.close();
        }
    }
}
