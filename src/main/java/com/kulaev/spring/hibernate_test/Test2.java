package com.kulaev.spring.hibernate_test;

import com.kulaev.spring.hibernate_test.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test2 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();
        try {
            Session session = factory.getCurrentSession();
            Employee employee = new Employee("Oleg", "Fedorov", "Security", 300);
            session.beginTransaction();
            session.save(employee);
            //session.getTransaction().commit();
            int myId = employee.getId();
           // Session session1 = factory.getCurrentSession();
            //session1.beginTransaction();
            Employee employee1= session.get(Employee.class,myId);
            session.getTransaction().commit();
            System.out.println(employee1);
            System.out.println("All done well");
        } finally {
            factory.close();
        }
    }
}
