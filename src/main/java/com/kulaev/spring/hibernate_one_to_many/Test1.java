package com.kulaev.spring.hibernate_one_to_many;

import com.kulaev.spring.hibernate_one_to_many.Entity.Department;
import com.kulaev.spring.hibernate_one_to_many.Entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test1 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Department.class)
                .buildSessionFactory();
        Session session =null;
        try {
//            session = factory.getCurrentSession();
//            session.beginTransaction();
//            Employee employee = session.get(Employee.class,4);
//            session.delete(employee);
//            session.getTransaction().commit();
//            System.out.println("All done well");
//*******************************************************************************************************************
            //session = factory.getCurrentSession();
           // session.beginTransaction();
            //Employee employee = session.get(Employee.class,1);
            //System.out.println(employee);
            //System.out.println(employee.getDepartment());
            //session.getTransaction().commit();
            //System.out.println("All done well");

//*******************************************************************************************************************
            session = factory.getCurrentSession();
            session.beginTransaction();
            System.out.println("Get Depatment");
            Department department = session.get(Department.class,4);
            System.out.println("Show department");
            System.out.println(department);
            System.out.println("Load employees Of the department");
            department.getEmployees().get(0);
            session.getTransaction().commit();
            System.out.println("Show employees Of the department");
            System.out.println(department.getEmployees());
            System.out.println("All done well");

//*******************************************************************************************************************
//            session = factory.getCurrentSession();
//            Department department = new Department("Philosophy",600,1500);
//            Employee employee1 = new Employee("Sergey","Kulaev",800);
//            Employee employee2 = new Employee("Dmitriy","Ivanov",600);
//            Employee employee3 = new Employee("Sancho","Pancho",1500);
//            department.addEmployeeToDepartment(employee1);
//            department.addEmployeeToDepartment(employee2);
//            department.addEmployeeToDepartment(employee3);
//            session.beginTransaction();
//            session.save(department);
//            session.getTransaction().commit();
//            System.out.println("All done well");
        }
        finally {
            session.close();
            factory.close();
        }
    }
}
